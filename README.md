# 家谱

#### 介绍
**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
本软件是一个家谱管理系统，本系统架构为vue3+antd+thinkphp6
本系统包括：家族成员统计、家族备份管理、家族成员管理、成员关系查询、名人轶事、家族风采（祠堂、相册、视频）、家族纪事功能。
本系统是一个轻量级小型管理系统，是一个适合学习的实习项目。
后端源码：https://gitee.com/shanhe/jiapu-admin


#### 安装教程

1.  下载node，适用于vue3的node版本，本系统开发是基于v14.12.0
2.  npm install/cnpm install
3.  npm run serve
4.  在线体检地址：http://www.hahaguai.top/ 账号：admin 密码：654321
#### 使用说明

<img src="./public/1.png" alt="" />
<img src="./public/2.png" alt="" />
<img src="./public/3.png" alt="" />
<img src="./public/4.png" alt="" />
<img src="./public/5.png" alt="" />
<img src="./public/6.png" alt="" />
<img src="./public/7.png" alt="" />
<img src="./public/8.png" alt="" />
<img src="./public/9.png" alt="" />
<img src="./public/10.png" alt="" />

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


