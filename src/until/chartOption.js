var option = {
    backgroundColor:'rgba(22, 90, 99, 0.5)',
    tooltip: {
    trigger: 'item',
    triggerOn: 'mousemove'
    },
    series: [
    {
        type: 'tree',
        orient: 'vertical',
        name: 'tree1',
        top: '5%',
        left: '7%',
        bottom: '2%',
        roam: true,
        symbolSize: 15,
        symbol: 'roundRect',
        edgeShape: 'polyline',
        symbolOffset:[0, 0],
        label: {
            normal: {
                position: "bottom",
                verticalAlign: "bottom",
                align: "center",
                fontSize: 12,
                formatter: param => {
                    let name = param.data.name;
                    if (name.length >= 10) {
                        name = name.slice(0, 9) + "...";
                    }
                    name = name.split('').join("\n");
                    return name;
                }
            }
        },
        leaves: {
            label: {
                normal: {
                    position: "bottom",
                    verticalAlign: "bottom",
                    align: "center",
                    distance: 10
                }
            }
        },
        emphasis: {
        focus: 'descendant'
        },
        expandAndCollapse: true,
        animationDuration: 550,
        animationDurationUpdate: 750,
        initialTreeDepth:100
    },
    ]
};

export default option;