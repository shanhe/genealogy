import {message} from 'ant-design-vue';
export function messageTips(tips,type) {    
    message.config({top: '48%',});
    switch(type){
        case 1:    
            message.error(tips);
        break;
        case 2:
            message.warning(tips);  
        break;
        case 3:
            message.success(tips);   
        break;
        default:break;
    }
}


export function checkIsInt(nubmer) { 
    var re = parseInt(nubmer).toString() === 'NaN';
    return !re;
}


/**
 * 手机号码正则验证
 * @param value
 */
export function isPhone(value) {
    var reg = /^1[0-9]{10}$/;
    if (!reg.test(value)) {
        return false;
    } else {
        return true;
    }
}

/**
 * 邮箱正则验证
 * @param value
 */
export function isEmail(value) {
    var reg = /^[0-9a-zA-Z_.-]+[@][0-9a-zA-Z_.-]+([.][a-zA-Z]+){1,2}$/;
    if (!reg.test(value)) {
        return false;
    } else {
        return true;
    }
}

//获取cookie
export function getCookie(sname){
    var acookie = document.cookie.split("; ");
    for (var i = 0; i < acookie.length; i++) {
        var arr = acookie[i].split("=");
        if (sname === arr[0]) {
            if (arr.length > 1){
                var sid = unescape(arr[1]);
                if(sname === 'z_sid'){
                    setCookie(sname,sid,{time:1});
                }
                return sid;
            } else{
                return "";
            }
        }
    }
    return "";
}
//设置cookie
export function setCookie(name,value,option){
    var seconds = option.time*24*60*60 || 0;
    var expires = "";
    if (seconds !== 0 ) {      //设置cookie生存时间
        var date = new Date();
        date.setTime(date.getTime()+(seconds*1000));
        expires = "; expires="+date.toGMTString();
    }
    var path = option.path||'/';
    document.cookie = name+"="+escape(value)+expires+"; path="+path;
}
//删除cookie
export function delCookie(name) {
    setCookie(name,'',{time:-1});
}

export function setSessionItem(key, value) {
    if (typeof (value) !== 'string') {
        value = JSON.stringify(value);
    }
    return window.sessionStorage.setItem(key, value);
}
export function getSessionItem(key) {
    var string = window.sessionStorage.getItem(key);
    try {
        var obj = JSON.parse(string);
        if (typeof obj === 'object' && obj) {
            return obj;
        } else {
            if (string === 'null') {
                return null;
            } else {
                return string;
            }
        }
    } catch (e) {
        if (string === 'null') {
            return null;
        } else {
            return string;
        }
    }
}
export function delSessionItem(name) {
    window.sessionStorage.removeItem(name);
}



/**
 * 昵称正则验证
 * @param value
 * 支持中英文、数字、“_”或减号
 */
 export function isName(value) {
    var myregex =/^[0-9A-Za-z\u4e00-\u9fa5_-]+$/;
    if (!myregex.test(value)){
        return false;
    }else{
        return true;
    }
}