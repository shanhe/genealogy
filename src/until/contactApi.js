const CONTACT_API = {
    logout:{
        url:'admin/index/logout',
        method:'get'
    },
    login:{
        url:'admin/login/index',
        method:'post',
        noTrans:true
    },
    updatePassword:{
        url:'admin/index/updatePassword',
        method:'post',
        noTrans:true
    },
    getGenerationList:{
        url:'admin/generation/index',
        method:'get'
    },
    addGeneration:{
        url:'admin/generation/add',
        method:'post',
        noTrans:true
    },
    updateGeneration:{
        url:'admin/generation/update',
        method:'post',
        noTrans:true
    },
    delGeneration:{
        url:'admin/generation/del',
        method:'get'
    },    
    getAllMember:{
        url:'admin/member/getAll',
        method:'get'
    },
    addMember:{
        url:'admin/member/add',
        method:'post',
        noTrans:true
    },
    editMember:{
        url:'admin/member/edit',
        method:'post',
        noTrans:true
    },
    getMemberList:{
        url:'admin/member/index',
        method:'get'
    },
    searchParent:{
        url:'admin/member/searchList',
        method:'get'
    },
    delMember:{
        url:'admin/member/del',
        method:'get'
    },
    delMemberAndChild:{
        url:'admin/member/delMemberAndChild',
        method:'get'
    },
    getSinglePageContent:{
        url:'admin/elegant/getSinglePage',
        method:'get'
    },
    updateSinglePage:{
        url:'admin/elegant/updateSinglePage',
        method:'post',
        noTrans:true
    },
    getPhotoAlbums:{
        url:'admin/elegant/getPhotoAlbums',
        method:'get'
    },
    getAlbumPics:{
        url:'admin/elegant/getAlbumPics',
        method:'get'
    },
    deleteAlbumImg:{
        url:'admin/elegant/deleteImg',
        method:'get'
    },
    editAlbumImgInfo:{
        url:'admin/elegant/editImgInfo',
        method:'post',
        noTrans:true
    },
    delAlbum:{
        url:'admin/elegant/delAlbum',
        method:'get'
    },
    getVideoList:{
        url:'admin/video/index',
        method:'get'
    },
    delVideo:{
        url:'admin/video/del',
        method:'get'
    },
    addFamous:{
        url:'admin/famous/add',
        method:'post',
        noTrans:true
    },
    editFamous:{
        url:'admin/famous/update',
        method:'post',
        noTrans:true
    },
    delFamous:{
        url:'admin/famous/del',
        method:'get'
    },
    getFamous:{
        url:'admin/famous/index',
        method:'get'
    },
    getFamousDetail:{
        url:'admin/famous/getDetail',
        method:'get'
    },
    addInstantbook:{
        url:'admin/instantbook/add',
        method:'post',
        noTrans:true
    },
    editInstantbook:{
        url:'admin/instantbook/update',
        method:'post',
        noTrans:true
    },
    delInstantbook:{
        url:'admin/instantbook/del',
        method:'get'
    },
    getInstantbook:{
        url:'admin/instantbook/index',
        method:'get'
    },
    getInstantbookDetail:{
        url:'admin/instantbook/getDetail',
        method:'get'
    },
    getCount:{
        url:'admin/member/getCount',
        method:'get'
    },
    getTodayBirthMember:{
        url:'admin/member/getTodayBirth',
        method:'get'
    },
    getFathers:{
        url:'admin/member/getFathers',
        method:'get'
    },
    getChildren:{
        url:'admin/member/getChildren',
        method:'get'
    },
    getRelationship:{
        url:'admin/member/getRelationship',
        method:'get'
    }
}
export default CONTACT_API;