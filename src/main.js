import { createApp } from 'vue'
import router from './router/router';
import App from './App.vue'
import antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import './scss/iconfont.css';

createApp(App).use(router).use(antd).mount('#app')
